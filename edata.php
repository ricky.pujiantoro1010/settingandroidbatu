<?php
require_once("confignew.php");

class edata {

    function openDB($db = '') {
        global $epConfig;
        if($db==''){$db=$epConfig['dbname_ebudget'];}
        $_server = $epConfig['server'];
        $_dbname = $db;
        $_dbport = $epConfig['dbport'];
        $_dbuser = $epConfig['dbuser'];
        $_dbpass = $epConfig['dbpass'];
        $con = pg_connect("host=$_server port=$_dbport dbname=$_dbname user=$_dbuser password=$_dbpass");
        return $con;
    }

    function closeDB($db) {
        $cls = pg_close($db);
        $log = debug_backtrace();
        if (!$cls) {
                $this->logMessage($log);
        }
        return $cls;
    }

    function rekening($usk_id) {
        // if ($usk_id == 0) {
            // return $this->get_datas("SELECT rekening_code,rekening_name FROM rekening WHERE LENGTH(rekening_code)=11 and substr(rekening_code,1,5)='5.2.2'");
            return $this->get_datas("SELECT rekening_code,rekening_name FROM rekening WHERE LENGTH(rekening_code)=11 and substr(rekening_code,1,1)='5'");
        /*}else{
            return $this->get_datas("SELECT rekening_code,rekening_name FROM rekening WHERE LENGTH(rekening_code)=11 and substr(rekening_code,1,5)='5.2.2' and rekening_code not in ('5.2.2.01.01')");
        }*/
    }

    function get_rekening() {
            return $this->get_datas("SELECT rekening_code,rekening_name FROM rekening WHERE LENGTH(rekening_code)=11 and substr(rekening_code,1,1)='5'");
    }

    /**
     * in  - string query
     * out - true or erre
     */
    function exec_query($query = null) {
        $db = $this->openDB();
        $log = debug_backtrace();
        $q = pg_query($query); //or die('Invalid Syntax');
        if (!$q) {
            $this->logMessage($log);
        }
        $this->closeDB($db);
        return $q;
    }

    /** data untuk tabel (banyak baris)
     * in  - string query
     * out - result array orobject
     * example in use - (banyak data, banyak row)
     * $data = get_datas("select * from table");
     * foreach($data as $data){
     *      echo $data['nama_kolom'];
     * }
     */
    function get_datas($query = null) {
        $log = debug_backtrace();
        $db = $this->openDB();
        $r = array();
        $q = pg_query($query);
        if (!$q) {
            $this->logMessage($log);
        }
        while ($s = pg_fetch_array($q)) {
            array_push($r, $s);
        }
        $this->closeDB($db);
        return (count($r) > 0) ? $r : array();
    }


    /**
     * data untuk tampilan (hanya satu baris/row) / bukan untuk baris tabel
     * in  - string query
     * out - result array orobject
     * example use -
     * (single data, satu row)
     * $data = get_data("select * from table");
     * echo $data['nama_kolom']; atau echo $data[0,1,2,etc]
     *
     * (banyak data, banyak row)
     * $data = get_data("select * from table where id=3");
     * echo $data['nama_kolom1'];
     * echo $data['nama_kolom2'];
     *
     */
    function get_data($query = null) {
        $log = debug_backtrace();
        $db = $this->openDB();
        $r = array();
        $q = pg_query($query); // or die('Can\'t Retrieve Data');
        if (!$q) {
            $this->logMessage($log);
        }
        if (pg_num_rows($q) == 1) {
            $s = pg_fetch_object($q);
            $r = get_object_vars($s);
        }
        $this->closeDB($db);
        return (count($r) > 0) ? $r : array();
    }

    private function logMessage($log) {
        $arrLog = array_reverse($log);
        echo "<ul>";
        foreach ($arrLog as $msg) {
            echo "<li>Syntax Error at function <b>" . $msg['function'] . "</b> in file <b>" . $msg['file'] . "</b> on Line <b>" . $msg['line'] . "</b>.</li>";
        }
        echo "</ul>";
        exit();
    }


    function get_datas_unconnect($query) {
        $r = array();
        $q = pg_query($query); //or die('Can\'t Retrieve Data');
        $log = debug_backtrace();
        if (!$q) {
            $this->logMessage($log);
        }
        while ($s = pg_fetch_array($q)) {
            array_push($r, $s);
        }
        return (count($r) > 0) ? $r : array();
    }

    function get_data_unconnect($query) {
        $r = array();
        $q = pg_query($query); // or die('Can\'t Retrieve Data');
        $log = debug_backtrace();
        if (!$q) {
            $this->logMessage($log);
        }
        if (pg_num_rows($q) == 1) {
            $s = pg_fetch_object($q);
            $r = get_object_vars($s);
        }
        return (count($r) > 0) ? $r : array();
    }

    
    /**
     * in  - nama_id, nama_tabel
     * out - angka (id) maksimal
     */
    function get_maxid($column, $table) {
        $res = $this->get_data("SELECT MAX(" . $column . ") as max FROM " . $table);
        $rex = $res['max'] + 1;
        return $rex;
    }

}
