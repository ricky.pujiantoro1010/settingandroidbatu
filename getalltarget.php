<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();


$getcount = $db->get_datas("SELECT COUNT(DISTINCT kode_rup) as total
FROM perencanaan
WHERE
target is not null");

$result = array();
foreach ($getcount as $count) {

    $tmp = array(
        'total' => $count['total'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
