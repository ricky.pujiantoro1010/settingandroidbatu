<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idpeng = $_POST['idpengawas'];
$tahun = date('Y');

$getpaket = $db->get_datas("SELECT DISTINCT a.kode_rup, a.nama_paket, a.lang, a.long, a.pagu_paket
from pilah_paket a
inner join perencanaan b on b.kode_rup = a.kode_rup
where id_pengawas = $idpeng and target is not null and a.tahun = '$tahun'");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'pkt_id' => $paket['kode_rup'],
        'pkt_nama_paket' => $paket['nama_paket'],
        'pagu' => $paket['pagu_paket'],
        'lat' => $paket['lang'],
        'long' => $paket['long'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
