<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

//$kegid = $_POST['kegiatan'];
$idsatker = $_POST['idpd'];

$getpaket = $db->get_datas("SELECT DISTINCT kode_rup, nama_paket, tgl_mulai_perencanaan, tgl_akhir_perencanaan, id_pengawas
FROM pilah_paket
WHERE id_satker = $idsatker and tgl_mulai_perencanaan is not null and tahun = '2023'");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'pkt_id' => $paket['kode_rup'],
        'pkt_nama_paket' => $paket['nama_paket'],
        'dateStart' => $paket['tgl_akhir_perencanaan'],
        'dateEnd' => $paket['tgl_mulai_perencanaan'],
        'idp' => $paket['id_pengawas'],

    );
    array_push($result, $tmp);
}

echo json_encode($result);
