<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idpengawas = $_POST['idpengawas'];

$getpaket = $db->get_datas("SELECT id_pengawas, id_badanusaha, nama_lengkap, nik, nomor, email, alamat_lengkap, tempat_lahir, tgl_lahir, jk, foto_ktp, rkn_nama
FROM data_pengawas join json_badanusaha on id_badanusaha = psr_id where id_pengawas = $idpengawas");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'idpengawas' => $paket['id_pengawas'],
        'nik' => $paket['nik'],
        'namapengawas' => $paket['nama_lengkap'],
        'alamat' => $paket['alamat_lengkap'],
        'tempatL' => $paket['tempat_lahir'],
        'tanggalL' => $paket['tgl_lahir'],
        'jk' => $paket['jk'],
        'nomor' => $paket['nomor'],
        'email' => $paket['email'],
        'fotoktp' => $paket['foto_ktp'],
        'idbu' => $paket['id_badanusaha'],
        'badanusaha' => $paket['rkn_nama'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
