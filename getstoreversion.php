<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$getpaket = $db->get_datas("SELECT versi
from versi_app");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'versi' => $paket['versi'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
