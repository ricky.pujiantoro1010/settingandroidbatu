<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idrup = $_POST['idrup'];

$getpaket = $db->get_datas("Select distinct a.kode_rup, a.nama_paket
from sirup_all a inner join pilah_paket b on b.kode_rup = a.kode_rup
inner join perencanaan c on b.kode_rup = c.kode_rup
where kode_rup = $idrup and target is not null");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'pkt_id' => $paket['kode_rup'],
        'pkt_nama_paket' => $paket['nama_paket'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);

