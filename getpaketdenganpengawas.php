<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

//$kegid = $_POST['kegiatan'];
$idsatker = $_POST['idpd'];

$getpaket = $db->get_datas("SELECT DISTINCT kode_rup, nama_paket, id_pengawas, file_bast
FROM sirup_all
INNER JOIN pilah_paket ON kode_rup = id_rup
WHERE id_satker = $idsatker and id_pengawas is not null");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'pkt_id' => $paket['kode_rup'],
        'pkt_nama_paket' => $paket['nama_paket'],
        'idpengawas' => $paket['id_pengawas'],
        'bast' => $paket['file_bast'],

    );
    array_push($result, $tmp);
}

echo json_encode($result);
