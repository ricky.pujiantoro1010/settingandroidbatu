<?php
include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

//$uk_id = $_GET['psk_uk_id'];

$datausu = $db->get_datas("SELECT psr_id, rkn_nama, rkn_npwp FROM json_badanusaha");

$result = array();
foreach ($datausu as $detail) {

    $tmp = array(
        'id_bu' => $detail['psr_id'],
        'nama_usaha' => $detail['rkn_nama'],
        'npwp' => $detail['rkn_npwp'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
