<?php
include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idpaket = $_POST['idPkt'];

$getminggu = $db->get_datas("SELECT id_rencana, bulan, minggu
FROM perencanaan
WHERE kode_rup = $idpaket
order by minggu asc");

$result = array();
foreach ($getminggu as $minggu) {

  $tmp = array(
    'idr' => $minggu['id_rencana'],
    'week' => $minggu['minggu'],
    'month' => $minggu['bulan'],
  );
  array_push($result, $tmp);
}

echo json_encode($result);
