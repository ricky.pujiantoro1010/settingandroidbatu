<?php

/**
 * created by yaqin jabrek
 * email : yaqindev@gmail.com
 */

class objek {

    // var $db, $fnc;

    function __construct() {
        $this->db  = new db();
        $this->edt = new edata();
    }

    /** fungsi untuk mendapatkan total komponen HSPK
     * in  - string komponen_id
     * out - number total komponen hspk
     * example in use - ('komponen_id')
     * $total = totalHspk("02.02.02.01.01");
     * echo $total;
     */
    function totalHspk($idk)
    {
        $tmpHarga    = $this->edt->get_data("SELECT SUM(a.koefisien * b.ssh_harga) AS total FROM komponen_member a INNER JOIN ssh b ON a.id_ssh = b.id_ssh WHERE a.komponen_id = '". $idk ."'");
        
         return round($tmpHarga['total'], 0);
    }

    /** fungsi untuk mendapatkan total komponen ASB Fisik
     * in  - string komponen_id
     * out - number total komponen asb fisik
     * example in use - ('komponen_id')
     * $total = totalASBFisik("02.02.02.01.01");
     * echo $total;
     */
    function totalASBFisik($idk)
    {
        $asb = $this->edt->get_datas("SELECT member_tipe, member_id, koefisien, id_ssh FROM komponen_member WHERE komponen_id = '$idk' ORDER BY member_tipe ASC, member_id ASC");

        $hrgTotal = 0;
        foreach ($asb as $value) {
            if ($value['member_tipe'] == 'HSPK') {
                
                $sumHarga = $this->edt->get_data("SELECT SUM(a.koefisien * b.ssh_harga) AS harga_total FROM komponen_member a INNER JOIN ssh b ON a.id_ssh = b.id_ssh WHERE a.komponen_id = '".$value['member_id']."' LIMIT 1");
                
                $jumlah   = $sumHarga['harga_total'] * $value['koefisien'];
                $hrgTotal = $hrgTotal + $jumlah;                  

            }else if ($value['member_tipe'] == 'SSH') {
                
                $row = $this->edt->get_data("SELECT ssh_harga FROM ssh WHERE komponen_id = '".$value['member_id']."' AND id_ssh = ". $value['id_ssh'] ." LIMIT 1");
                
                if (count($row) > 0) {
                    $jumlah   = $row['ssh_harga'] * $value['koefisien'];
                    $hrgTotal = $hrgTotal + $jumlah;  
                }
            }
        }
        return round($hrgTotal, -2);
    }

    /** fungsi untuk mendapatkan total komponen ASB Non Fisik
     * in  - string komponen_id
     * out - number total komponen asb fisik
     * example in use - ('komponen_id')
     * $total = totalASBNonFisik("02.02.02.01.01");
     * echo $total;
     */
    function totalASBNonFisik( $idk, $arrParam = array(), $tipe = '1', $arrPendukung = array() )
    {
        $keg = $this->edt->get_data("SELECT keg_parameter AS param, keg_satuan AS satuan ,keg_value as valuee FROM sub_kegiatan WHERE  keg_id = '$idk' LIMIT 1");
        if (empty($keg)) {
            return 0 ;
            exit;
        }
        $tmpParam  = (count(explode('|', $keg['param'])) > 0) ? explode('|', $keg['param']) : array();
        $tmpSatuan = (count(explode('|', $keg['satuan'])) > 0) ? explode('|', $keg['satuan']) : array();
        $tmpValue  = (count(explode('|', $keg['valuee'])) > 0) ? explode('|', $keg['valuee']) : array();
        $jumTot    = 0;


        // tipe ini untuk mengecek tipe swakelola atau paket penyedia 
        // 0 untuk penyedia
        // 1 untuk swakelola
        if ($tipe == 1){
            $tmp_sub = $this->edt->get_datas("select * from sub_kegiatan_member where keg_id = '$idk'");
        } else {
            $tmp_sub = $this->edt->get_datas("select * from sub_kegiatan_member where keg_id = '$idk' and sub_keg_tipe = '$tipe'");
            
        }
        
        if (count($tmp_sub) > 0 ){
            foreach($tmp_sub as $v) {
                $idssh = $v['id_ssh'];

                $tmp_ssh = $this->edt->get_data("SELECT * from ssh WHERE id_ssh = $idssh limit 1");
                
                $harga = (isset($tmp_ssh['ssh_harga'])) ? $tmp_ssh['ssh_harga'] : '';
                $arr_index = ($v['sub_keg_parameter'] != '') ? explode('|', $v['sub_keg_parameter']) : array();
                $tipeRumus = $v['sub_keg_tipe_rumus'];
                if (count($arr_index) > 0 ) {
                    $jumlahSatuan = 1;
                    if ($tipeRumus == '1') {
                        for ($i=0; $i < count($arr_index); $i++) {
                            $valuue = (count($arrParam) > 0) ? $arrParam : $tmpValue;
                            $jumlahSatuan = $jumlahSatuan * $valuue[$arr_index[$i]];
                        }
                    } elseif($tipeRumus == '2') {
                            $valuue = (count($arrParam) > 0) ? $arrParam : $tmpValue;
                            $jumlahSatuan = ( $valuue[$arr_index[0]] + $valuue[$arr_index[1]] ) * $valuue[$arr_index[2]];
                        
                    }elseif ($tipeRumus == '3') {
                        $jumlahSatuan = 0;
                        for ($i=0; $i < count($arr_index); $i++) {
                            $valuue = (count($arrParam) > 0) ? $arrParam : $tmpValue;
                            $jumlahSatuan = $jumlahSatuan + $valuue[$arr_index[$i]];
                        }
                    }
                    $total = $jumlahSatuan * $harga;
                    $jumTot += $total;
                } else {
                    $jumTot += $harga;
                }
            } 
        }
        return round($jumTot,-2);
    }

    /** fungsi untuk mendapatkan total komponen ASB Non Fisik + komponen tambahan
     * in  - interger usulan id
     * out - number total komponen asb fisik + komponen tambahan
     * example in use - ('usulan id')
     * $total = totalASBNonFisikDanTambahan(1);
     * echo $total;
     */
    function totalASBNonFisikDanTambahan( $usulanId )
    {
        // GET DATA USULAN
        $usulan = $this->db->get_data("SELECT usu_kompid AS id FROM msr_mstrusulan WHERE usu_id = $usulanId");
        // SET KOMPONEN ID NON FISIK
        $komponenIdAsbNonFisik = $usulan['id'];
        // SET ARRAY PARAMETER VOLUME NON FISIK
        $komponen              = $this->db->get_data("SELECT tmb_volume AS volume FROM msr_komponentambahannonfisik WHERE tmb_komponenid = '$komponenIdAsbNonFisik' AND tmb_usu_id = $usulanId");
        $arrVolume             = ( $komponen['volume'] != '' ) ? explode('|', $komponen['volume']) : array();
        // GET TOTAL ASB NO FISIK
        $totalAsbNOnFisik = $this->totalASBNonFisik($komponenIdAsbNonFisik, $arrVolume);
        // SET DEFAULT TOTAL KOMPONEN TAMBAHAN
        $totalKomponenTambahan = 0;
        // GET LIST KOMPONEN DARI TABEL msr_komponentambahanonfisik
        $listKomponen = $this->db->get_datas("SELECT tmb_komponenid AS komponen_id, tmb_volume AS volume FROM msr_komponentambahannonfisik WHERE tmb_usu_id = $usulanId and tmb_komponenid NOT IN ('$komponenIdAsbNonFisik')");
        foreach ($listKomponen as $val) {
            // GET DATA KOMPONEN
            $data = $this->edt->get_data("SELECT komponen_harga AS harga FROM komponen WHERE komponen_id = '". $val['komponen_id'] ."' LIMIT 1 ");
            // GET HARGA
            $harga = abs($data['harga']);
            // TOTAL = HARGA X VOLUME
            $total = $harga * abs( $val['volume'] );
            $totalKomponenTambahan += $total;
        }
        // JUMLAH TOTAL ASB NON FISIK + TOTAL KOMPONEN TAMBAHAN
        $total = $totalAsbNOnFisik + $totalKomponenTambahan;
        // RETURN TOTAL
        return $total;
    }

    /** fungsi untuk mendapatkan detail komponen ASB Fisik
     * in  - string komponen_id
     * out - html komponen ASB Fisik
     * example in use - ('komponen_id')
     * echo $namaClass->getDetailAsbFisik("02.02.02.01.01");
     */


    /*searching objek*/
    /** fungsi untuk mencari objek yang di cari
     * in  - string keyword, string rekening
     * out - array list objek
     * example in use - ('kata kunci', 'kode rekening')
     * $objek = srcKomponen("ballpoint", '5.2.2.01.01');
     * echo $objek['idSsh'];
     * echo $objek['kmpId'];
     * echo $objek['kmpName'];
     * echo $objek['hrgRp'];
     */
    public function srcKomponen($keyword = '', $rekening = '0')
    {
        if ($rekening <> '0') {
            $data = $this->edt->get_datas("SELECT 
                                                id_ssh AS ids, 
                                                b.komponen_id AS idk, 
                                                komponen_name AS nama, 
                                                komponen_harga AS harga, 
                                                komponen_tipe AS tipe, 
                                                satuan, 
                                                komponen_spesifikasi AS spesifikasi 
                                            FROM komponen_rekening a 
                                            INNER JOIN komponen b ON a.komponen_id = b.komponen_id 
                                            WHERE 
                                                (komponen_name ILIKE '%" . $keyword . "%' 
                                                OR komponen_spesifikasi ILIKE '%" . $keyword . "%') 
                                                AND a.rekening_code = '$rekening' 
                                                AND komponen_show = 't' 
                                                AND komponen_tipe != 'ASB NF'
                                            ORDER BY komponen_name ASC, komponen_harga ASC
                                            LIMIT 250");
        } else {
            $data = $this->edt->get_datas(" SELECT  id_ssh AS ids, 
                                                    b.komponen_id AS idk, 
                                                    komponen_name AS nama, 
                                                    komponen_harga AS harga, 
                                                    satuan, 
                                                    komponen_tipe AS tipe 
                                                FROM 
                                                    komponen_rekening a 
                                                INNER JOIN komponen b ON a.komponen_id = b.komponen_id 
                                                WHERE 
                                                    (komponen_name ILIKE '%" . $keyword . "%'
                                                    OR komponen_spesifikasi ILIKE '%" . $keyword . "%') 
                                                    AND komponen_show = 't' AND 
                                                    komponen_tipe = 'ASB NF' 
                                                ORDER BY komponen_name ASC 
                                                LIMIT 250");
        }
        $word = array();
        if ($rekening === '') {
            $kata = array(
                    'idSsh'     => '', 
                    'kmpId'     => '', 
                    'kmpName'   => '',
                    'hrgRp'     => ''
                );
            array_push($word, $kata);
        }else{
            foreach ($data as $val) {
                $tipe   = $val['tipe'];
                $harga  = 0;
                $satuan = '';
                if ($tipe == 'ASB FISIK') {
                    $harga  = $this->totalASBFisik( $val['idk'] );
                    $satuan = $val['satuan'];
                }else if ($tipe == 'ASB NF'){
                    $harga  = $this->totalASBNonFisik( $val['idk'] );
                    $satuan = 'Kegiatan';
                }else {
                    $harga  = $val['harga'];
                    $satuan = $val['satuan'];
                }
                if($val['spesifikasi']!=""){
                    $nama=$val['nama'].", ".$val['spesifikasi'];
                    $namakomponen=(strlen($nama)>=150)?mb_substr($nama,0,100)."...":$nama;
                }else{
                    $namakomponen=(strlen($val['nama'])>=150)?mb_substr($val['nama'],0,100)."...":$val['nama'];
                }
                $kata = array(
                    'idSsh'     => $val['ids'], 
                    'kmpId'     => $val['idk'], 
                    'kmpName'   => $namakomponen,
                    'hrgRp'     => 'Rp ' . number_format($harga, 2, '.', ',') . " / " . $satuan
                );

                array_push($word, $kata);
            }
        }
        return $word;
    }
    
}
