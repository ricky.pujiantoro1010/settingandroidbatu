<?php
require_once("confignew.php");

class db
{

    function openDB($db = '')
    {
        global $epConfig;
        if ($db == '') {
            $db = $epConfig['dbname_eplanning'];
        }
        $_server = $epConfig['server'];
        $_dbname = $db;
        $_dbport = $epConfig['dbport'];
        $_dbuser = $epConfig['dbuser'];
        $_dbpass = $epConfig['dbpass'];
        $con = pg_connect("host=$_server port=$_dbport dbname=$_dbname user=$_dbuser password=$_dbpass");
        return $con;
    }

    function closeDB($db)
    {
        $cls = pg_close($db);
        $log = debug_backtrace();
        if (!$cls) {
            $this->logMessage($log);
        }
        return $cls;
    }

    /**
     * List icon font-Awesome
     * @return array with index label and data contains icon's of name ex : fa-home, fa-archive
     */
    function listIcon()
    {
        $icon = array();

        $icon[] = array(
            'label' => 'General Icon',
            'data' => array('fa-automobile', 'fa-bank', 'fa-behance', 'fa-behance-square', 'fa-bomb', 'fa-building', 'fa-cab', 'fa-car', 'fa-child', 'fa-circle-o-notch', 'fa-circle-thin', 'fa-codepen', 'fa-cube', 'fa-cubes', 'fa-database', 'fa-delicious', 'fa-deviantart', 'fa-digg', 'fa-drupal', 'fa-empire', 'fa-envelope-square', 'fa-fax', 'fa-file-archive-o', 'fa-file-audio-o', 'fa-file-code-o', 'fa-file-excel-o', 'fa-file-image-o', 'fa-file-movie-o', 'fa-file-pdf-o', 'fa-file-photo-o', 'fa-file-picture-o', 'fa-file-powerpoint-o', 'fa-file-sound-o', 'fa-file-video-o', 'fa-file-word-o', 'fa-file-zip-o', 'fa-ge', 'fa-git', 'fa-git-square', 'fa-google', 'fa-graduation-cap', 'fa-hacker-news', 'fa-header', 'fa-history', 'fa-institution', 'fa-joomla', 'fa-jsfiddle', 'fa-language', 'fa-life-bouy', 'fa-life-ring', 'fa-life-saver', 'fa-mortar-board', 'fa-openid', 'fa-paper-plane', 'fa-paper-plane-o', 'fa-paragraph', 'fa-paw', 'fa-pied-piper', 'fa-pied-piper-alt', 'fa-pied-piper-square', 'fa-qq', 'fa-ra', 'fa-rebel', 'fa-recycle', 'fa-reddit', 'fa-reddit-square', 'fa-send', 'fa-send-o', 'fa-share-alt', 'fa-share-alt-square', 'fa-slack', 'fa-sliders', 'fa-soundcloud', 'fa-space-shuttle', 'fa-spoon', 'fa-spotify', 'fa-steam', 'fa-steam-square', 'fa-stumbleupon', 'fa-stumbleupon-circle', 'fa-support', 'fa-taxi', 'fa-tencent-weibo', 'fa-tree', 'fa-university', 'fa-vine', 'fa-wechat', 'fa-weixin', 'fa-wordpress', 'fa-yahoo')
        );
        $icon[] = array(
            'label' => 'Web Applications',
            'data' => array('fa-adjust', 'fa-anchor', 'fa-archive', 'fa-asterisk', 'fa-ban', 'fa-bar-chart-o', 'fa-barcode', 'fa-beer', 'fa-bell', 'fa-bell-o', 'fa-bolt', 'fa-book', 'fa-bookmark', 'fa-bookmark-o', 'fa-briefcase', 'fa-bug', 'fa-building', 'fa-bullhorn', 'fa-bullseye', 'fa-calendar', 'fa-calendar-o', 'fa-camera', 'fa-camera-retro', 'fa-caret-square-o-down', 'fa-caret-square-o-left', 'fa-caret-square-o-right', 'fa-caret-square-o-up', 'fa-certificate', 'fa-check', 'fa-check-circle', 'fa-check-circle-o', 'fa-check-square', 'fa-check-square-o', 'fa-circle', 'fa-circle-o', 'fa-clock-o', 'fa-cloud', 'fa-cloud-download', 'fa-cloud-upload', 'fa-code', 'fa-code-fork', 'fa-coffee', 'fa-cog', 'fa-cogs', 'fa-plus-square-o', 'fa-comment', 'fa-comment-o', 'fa-comments', 'fa-comments-o', 'fa-compass', 'fa-credit-card', 'fa-crop', 'fa-crosshairs', 'fa-cutlery', 'fa-dashboard', 'fa-desktop', 'fa-dot-circle-o', 'fa-download', 'fa-edit', 'fa-ellipsis-horizontal', 'fa-ellipsis-vertical', 'fa-envelope', 'fa-envelope-o', 'fa-eraser', 'fa-exchange', 'fa-exclamation', 'fa-exclamation-circle', 'fa-exclamation-triangle', 'fa-minus-square-o', 'fa-external-link', 'fa-external-link-square', 'fa-eye', 'fa-eye-slash', 'fa-female', 'fa-fighter-jet', 'fa-film', 'fa-filter', 'fa-fire', 'fa-fire-extinguisher', 'fa-flag', 'fa-flag-checkered', 'fa-flag-o', 'fa-flash', 'fa-flask', 'fa-folder', 'fa-folder-o', 'fa-folder-open', 'fa-folder-open-o', 'fa-frown-o', 'fa-gamepad', 'fa-gavel', 'fa-gear', 'fa-gears', 'fa-gift', 'fa-glass', 'fa-globe', 'fa-group', 'fa-hdd-o', 'fa-headphones', 'fa-heart', 'fa-heart-o', 'fa-home', 'fa-inbox', 'fa-info', 'fa-info-circle', 'fa-key', 'fa-keyboard-o', 'fa-laptop', 'fa-leaf', 'fa-legal', 'fa-lemon-o', 'fa-level-down', 'fa-level-up', 'fa-lightbulb-o', 'fa-location-arrow', 'fa-lock', 'fa-magic', 'fa-magnet', 'fa-mail-forward', 'fa-mail-reply', 'fa-mail-reply-all', 'fa-male', 'fa-map-marker', 'fa-meh-o', 'fa-microphone', 'fa-microphone-slash', 'fa-minus', 'fa-minus-circle', 'fa-minus-square', 'fa-minus-square-o', 'fa-mobile', 'fa-mobile-phone', 'fa-money', 'fa-moon-o', 'fa-move', 'fa-music', 'fa-pencil', 'fa-pencil-square', 'fa-pencil-square-o', 'fa-phone', 'fa-phone-square', 'fa-picture-o', 'fa-plane', 'fa-plus', 'fa-plus-circle', 'fa-plus-square', 'fa-power-off', 'fa-print', 'fa-puzzle-piece', 'fa-qrcode', 'fa-question', 'fa-question-circle', 'fa-quote-left', 'fa-quote-right', 'fa-random', 'fa-refresh', 'fa-reorder', 'fa-reply', 'fa-reply-all', 'fa-resize-horizontal', 'fa-resize-vertical', 'fa-retweet', 'fa-road', 'fa-rocket', 'fa-rss', 'fa-rss-square', 'fa-search', 'fa-search-minus', 'fa-search-plus', 'fa-share', 'fa-share-square', 'fa-share-square-o', 'fa-shield', 'fa-shopping-cart', 'fa-sign-in', 'fa-sign-out', 'fa-signal', 'fa-sitemap', 'fa-smile-o', 'fa-sort', 'fa-sort-alpha-asc', 'fa-sort-alpha-desc', 'fa-sort-amount-asc', 'fa-sort-amount-desc', 'fa-sort-asc', 'fa-sort-desc', 'fa-sort-down', 'fa-sort-numeric-asc', 'fa-sort-numeric-desc', 'fa-sort-up', 'fa-spinner', 'fa-square', 'fa-square-o', 'fa-star', 'fa-star-half', 'fa-star-half-empty', 'fa-star-half-full', 'fa-star-half-o', 'fa-star-o', 'fa-subscript', 'fa-suitcase', 'fa-sun-o', 'fa-superscript', 'fa-tablet', 'fa-tachometer', 'fa-tag', 'fa-tags', 'fa-tasks', 'fa-terminal', 'fa-thumb-tack', 'fa-thumbs-down', 'fa-thumbs-o-down', 'fa-thumbs-o-up', 'fa-thumbs-up', 'fa-ticket', 'fa-times', 'fa-times-circle', 'fa-times-circle-o', 'fa-tint', 'fa-toggle-down', 'fa-toggle-left', 'fa-toggle-right', 'fa-toggle-up', 'fa-trash-o', 'fa-trophy', 'fa-truck', 'fa-umbrella', 'fa-unlock', 'fa-unlock-alt', 'fa-unsorted', 'fa-upload', 'fa-user', 'fa-video-camera', 'fa-volume-down', 'fa-volume-off', 'fa-volume-up', 'fa-warning', 'fa-wheelchair', 'fa-wrench')
        );
        $icon[] = array(
            'label' => 'Currency Icons',
            'data' => array('fa-bitcoin', 'fa-btc', 'fa-cny', 'fa-dollar', 'fa-eur', 'fa-euro', 'fa-gbp', 'fa-inr', 'fa-jpy', 'fa-krw', 'fa-money', 'fa-rmb', 'fa-rouble', 'fa-rub', 'fa-ruble', 'fa-rupee', 'fa-try', 'fa-turkish-lira', 'fa-usd', 'fa-won', 'fa-yen')
        );
        $icon[] = array(
            'label' => 'Text Editor Icons',
            'data' => array('fa-align-center', 'fa-align-justify', 'fa-align-left', 'fa-align-right', 'fa-bold', 'fa-chain', 'fa-chain-broken', 'fa-clipboard', 'fa-columns', 'fa-copy', 'fa-cut', 'fa-dedent', 'fa-eraser', 'fa-file', 'fa-file-o', 'fa-file-text', 'fa-file-text-o', 'fa-files-o', 'fa-floppy-o', 'fa-font', 'fa-indent', 'fa-italic', 'fa-link', 'fa-list', 'fa-list-alt', 'fa-list-ol', 'fa-list-ul', 'fa-outdent', 'fa-paperclip', 'fa-paste', 'fa-repeat', 'fa-rotate-left', 'fa-rotate-right', 'fa-save', 'fa-scissors', 'fa-strikethrough', 'fa-table', 'fa-text-height', 'fa-text-width', 'fa-th', 'fa-th-large', 'fa-th-list', 'fa-underline', 'fa-undo', 'fa-unlink')
        );
        $icon[] = array(
            'label' => 'Directional icons',
            'data' => array('fa-angle-double-down', 'fa-angle-double-left', 'fa-angle-double-right', 'fa-angle-double-up', 'fa-angle-down', 'fa-angle-left', 'fa-angle-right', 'fa-angle-up', 'fa-arrow-circle-down', 'fa-arrow-circle-left', 'fa-arrow-circle-o-down', 'fa-arrow-circle-o-left', 'fa-arrow-circle-o-right', 'fa-arrow-circle-o-up', 'fa-arrow-circle-right', 'fa-arrow-circle-up', 'fa-arrow-down', 'fa-arrow-left', 'fa-arrow-right', 'fa-arrow-up', 'fa-caret-down', 'fa-caret-left', 'fa-caret-right', 'fa-caret-square-o-down', 'fa-caret-square-o-left', 'fa-caret-square-o-right', 'fa-caret-square-o-up', 'fa-caret-up', 'fa-chevron-circle-down', 'fa-chevron-circle-left', 'fa-chevron-circle-right', 'fa-chevron-circle-up', 'fa-chevron-down', 'fa-chevron-left', 'fa-chevron-right', 'fa-chevron-up', 'fa-hand-o-down', 'fa-hand-o-left', 'fa-hand-o-right', 'fa-hand-o-up', 'fa-long-arrow-down', 'fa-long-arrow-left', 'fa-long-arrow-right', 'fa-long-arrow-up', 'fa-toggle-down', 'fa-toggle-left', 'fa-toggle-right', 'fa-toggle-up')
        );
        $icon[] = array(
            'label' => 'Video Player icons',
            'data' => array('fa-backward', 'fa-eject', 'fa-fast-backward', 'fa-fast-forward', 'fa-forward', 'fa-arrows-alt', 'fa-pause', 'fa-play', 'fa-play-circle', 'fa-play-circle-o', 'fa-expand', 'fa-compress', 'fa-step-backward', 'fa-step-forward', 'fa-stop', 'fa-youtube-play')
        );
        $icon[] = array(
            'label' => 'Brands icons',
            'data' => array('fa-adn', 'fa-android', 'fa-apple', 'fa-bitbucket', 'fa-bitbucket-square', 'fa-bitcoin', '(alias)', 'fa-btc', 'fa-css3', 'fa-dribbble', 'fa-dropbox', 'fa-facebook', 'fa-facebook-square', 'fa-flickr', 'fa-foursquare', 'fa-github', 'fa-github-alt', 'fa-github-square', 'fa-gittip', 'fa-google-plus', 'fa-google-plus-square', 'fa-html5', 'fa-instagram', 'fa-linkedin', 'fa-linkedin-square', 'fa-linux', 'fa-maxcdn', 'fa-pagelines', 'fa-pinterest', 'fa-pinterest-square', 'fa-renren', 'fa-skype', 'fa-stack-exchange', 'fa-stack-overflow', 'fa-trello', 'fa-tumblr', 'fa-tumblr-square', 'fa-twitter', 'fa-twitter-square', 'fa-vimeo-square', 'fa-vk', 'fa-weibo', 'fa-windows', 'fa-xing', 'fa-xing-square', 'fa-youtube', 'fa-youtube-play', 'fa-youtube-square')
        );
        $icon[] = array(
            'label' => 'Medical icons',
            'data' => array('fa-ambulance', 'fa-h-square', 'fa-hospital-o', 'fa-medkit', 'fa-plus-square', 'fa-stethoscope', 'fa-user-md', 'fa-wheelchair')
        );
        return $icon;
    }

    /**
     * in  - string query
     * out - true or erre
     */
    function exec_query($query = null)
    {
        $db = $this->openDB();
        $log = debug_backtrace();
        $q = pg_query($query); //or die('Invalid Syntax');
        if (!$q) {
            $this->logMessage($log);
        }
        $this->closeDB($db);
        return $q;
    }

    /** data untuk tabel (banyak baris)
     * in  - string query
     * out - result array orobject
     * example in use - (banyak data, banyak row)
     * $data = get_datas("select * from table");
     * foreach($data as $data){
     * 		echo $data['nama_kolom'];
     * }
     */
    function get_datas($query = null)
    {
        $log = debug_backtrace();
        $db = $this->openDB();
        $r = array();
        $q = pg_query($query);
        if (!$q) {
            $this->logMessage($log);
        }
        while ($s = pg_fetch_array($q)) {
            array_push($r, $s);
        }
        $this->closeDB($db);
        return (count($r) > 0) ? $r : array();
    }

    function data_Resource($resource, $oneRow = TRUE)
    {
        $r = array();
        if ($oneRow) {
            $s = pg_fetch_object($resource);
            $r = get_object_vars($s);
        } else {
            while ($row = pg_fetch_array($resource)) {
                array_push($r, $row);
            }
        }
        return $r;
    }

    /**
     * data untuk tampilan (hanya satu baris/row) / bukan untuk baris tabel
     * in  - string query
     * out - result array orobject
     * example use -
     * (single data, satu row)
     * $data = get_data("select * from table");
     * echo $data['nama_kolom']; atau echo $data[0,1,2,etc]
     *
     * (banyak data, banyak row)
     * $data = get_data("select * from table where id=3");
     * echo $data['nama_kolom1'];
     * echo $data['nama_kolom2'];
     *
     */
    function get_data($query = null)
    {
        $log = debug_backtrace();
        $db = $this->openDB();
        $r = array();
        $q = pg_query($query); // or die('Can\'t Retrieve Data');
        if (!$q) {
            $this->logMessage($log);
        }
        if (pg_num_rows($q) == 1) {
            $s = pg_fetch_object($q);
            $r = get_object_vars($s);
        }
        $this->closeDB($db);
        return (count($r) > 0) ? $r : array();
    }

    /**
     * in  - nama_id, nama_tabel
     * out - angka (id) maksimal
     */
    function get_maxid($column, $table)
    {
        $res = $this->get_data("SELECT MAX(" . $column . ") as max FROM " . $table);
        $rex = abs($res['max']) + 1;
        return $rex;
    }

    // function unconnect --
    function exec_query_unconnect($query)
    {
        $q = pg_query($query); // or die('Invalid Syntax');
        $log = debug_backtrace();
        if (!$q)
            $this->logMessage($log);
        return $q;
    }

    function get_datas_unconnect($query)
    {
        $r = array();
        $q = pg_query($query); //or die('Can\'t Retrieve Data');
        $log = debug_backtrace();
        if (!$q) {
            $this->logMessage($log);
        }
        while ($s = pg_fetch_array($q)) {
            array_push($r, $s);
        }
        return (count($r) > 0) ? $r : array();
    }

    function get_data_unconnect($query)
    {
        $r = array();
        $q = pg_query($query); // or die('Can\'t Retrieve Data');
        $log = debug_backtrace();
        if (!$q) {
            $this->logMessage($log);
        }
        if (pg_num_rows($q) == 1) {
            $s = pg_fetch_object($q);
            $r = get_object_vars($s);
        }
        return (count($r) > 0) ? $r : array();
    }

    function get_maxid_unconnect($column, $table)
    {
        $res = $this->get_data_unconnect("SELECT MAX(" . $column . ") as max FROM " . $table . "");
        $rex = abs($res['max']) + 1;
        return $rex;
    }

    function get_maxid_table($column, $table)
    {
        $res = $this->get_data_unconnect("SELECT MAX(" . $column . ") as max FROM " . $table . "");
        if (count($res) > 0 || $res['max'] != '') {
            $rex = $res['max'] + 1;
            return $rex;
        } else {
            return 1;
        }
    }

    /**
     * <b>WARNING</b> Open DB FIRST <br/>
     * Fungsi untuk generate kode baru ex : 10.10.1 dst
     * @param String $columnKode Nama Kolom Kode di tabel @name $table
     * @param String $columnIdParent Nama Kolom id parent di tabel @name $table
     * @param String $table Nama tabel dari kode
     * @param String $kodeKepala Kode Kepala Sekarang sebagai induk
     * @param Integer $idParent Id Kepala Sekarang sebagai induk
     * @param Boolean $zeroBefore Default true: Memberikan angka 0 jika kode baru di bawah angka 10, false otherwise
     * @return String kode baru
     */
    function get_newkode($columnKode, $columnIdParent, $table, $kodeKepala, $idParent, $zeroBefore = TRUE)
    {
        $arrLis = array();
        $keg = $this->get_datas_unconnect("SELECT $columnKode FROM $table WHERE $columnIdParent = $idParent");
        foreach ($keg as $value) {
            array_push($arrLis, $value[$columnKode]);
        }
        $no = 1;
        while (true) {
            $no = ($zeroBefore && $no < 10) ? '0' . $no : $no;
            $new = $kodeKepala . "." . $no;
            if (!in_array($new, $arrLis)) {
                break;
            } else {
                array_push($arrLis, $new);
            }
            $no++;
        }
        return $new;
    }

    function getListSkpdCombo($idskpd = 0, $textFirst = '--Pilih Perangkat Daerah--')
    {
        echo '<option value="0">' . $textFirst . '</option>';
        $skpd = $this->get_datas("SELECT uk_id,uk_nama FROM msr_unitkerja ORDER BY uk_nama");
        foreach ($skpd as $value) {
            $sel = ($value['uk_id'] == $idskpd) ? 'selected' : '';
            echo "<option value='" . $value['uk_id'] . "' $sel>" . $value['uk_nama'] . "</option>";
        }
    }


    /**
     * Pastikan untuk memanggil fungsi openDb
     * Fungsi untuk menampilkan menu.<br/>
     *  array $explv 'name','parent','href','class','title','id','base','idm'
     * @global integer $explc Index global
     * @global array $explv
     * @param integer $parent Induk Menu, untuk list menu di awali dengan 0
     * @param integer $idakses id akses dari account
     * @param integer $base Untuk melihat level dari child
     */
    function navlist($parent, $idakses)
    {

        $explv = array();
        $query = $this->get_datas_unconnect("SELECT mnu_id,mnu_parent,mnu_nama,mnu_title,mnu_class,mnu_idtag,mnu_href FROM msr_menu, msr_menuakses WHERE mnu_id = mks_id_menu and mnu_parent =" . $parent . " AND mks_akses = $idakses ORDER BY mnu_urut");

        foreach ($query as $key => $value) {
            $arrProp = array(
                "name" => $value['mnu_nama'],
                "title" => $value['mnu_title'],
                "url" => $value['mnu_href'],
                "id" => $value['mnu_idtag'],
                "icon" => $value['mnu_class'],
                "idm" => $value['mnu_id'],
                "sub" => $this->navlist($value['mnu_id'], $idakses)
            );
            if ($key == 0 && $parent == 0) {
                $arrProp['active'] = 'active';
            }
            if (empty($value['mnu_class'])) {
                unset($arrProp['icon']);
            }
            if (empty($value['mnu_idtag'])) {
                unset($arrProp['id']);
            }
            if (empty($value['mnu_href'])) {
                unset($arrProp['url']);
            }
            if (count($arrProp['sub']) == 0) {
                unset($arrProp['sub']);
            }
            $explv[$value['mnu_nama']] = $arrProp;
        }
        return $explv;
    }

    /**
     * Pastikan untuk memanggil fungsi openDb
     * Fungsi untuk mengambil nama jabatan berdasarkan parent masing2 jabatan.<br/>
     *  array $explv Index : 0 = idjab;1 = parent; 2 = kode_jabatan; 3 =nama_jabatan; 4=jabatan; 5 = id unit organasasi; 6 = id_golongan;7 = status ttp jabatan ;11 = base
     * @global integer $explc Index global
     * @global array $explv
     * @param integer $uk Unit Kerja
     * @param integer $parent Induk Jabatan, untuk list jabatan di awali dengan 0
     * @param integer $base Untuk melihat level dari child
     */
    function explore($uk, $parent, $base)
    {
        global $explc;
        global $explv;
        if ($parent == 0) {
            $query = $this->get_datas_unconnect("SELECT idjab,parent,kode_jabatan,nama_jabatan,jabatan,unit_organisasi, golongan,sts_ttp FROM jabatan WHERE parent=" . $parent . " AND unit_kerja=" . $uk . " ORDER BY urutjab");
        } else {
            $query = $this->get_datas_unconnect("SELECT idjab,parent,kode_jabatan,nama_jabatan,jabatan, unit_organisasi, golongan,sts_ttp FROM jabatan WHERE parent=" . $parent . " ORDER BY urutjab");
        }
        $explc++;
        foreach ($query as $value) {
            $explv[$explc][0] = $value['idjab'];
            $explv[$explc][1] = $value['parent'];
            $explv[$explc][2] = $value['kode_jabatan'];
            $explv[$explc][3] = $value['nama_jabatan'];
            $explv[$explc][4] = $value['jabatan'];
            $explv[$explc][5] = $value['unit_organisasi'];
            $explv[$explc][6] = $value['golongan'];
            $explv[$explc][7] = $value['sts_ttp'];

            $explv[$explc][11] = $base;
            $base++;
            $this->explore($uk, $value['idjab'], $base);
            $base--;
        }
    }

    private function logMessage($log)
    {
        $arrLog = array_reverse($log);
        echo "<ul>";
        foreach ($arrLog as $msg) {
            echo "<li>Syntax Error at function <b>" . $msg['function'] . "</b> in file <b>" . $msg['file'] . "</b> on Line <b>" . $msg['line'] . "</b>.</li>";
        }
        echo "</ul>";
        exit();
    }

    function zip_error_message($errno)
    {
        // using constant name as a string to make this function PHP4 compatible
        $zipFileFunctionsErrors = array(
            'ZIPARCHIVE::ER_MULTIDISK'   => 'Multi-disk zip archives not supported.',
            'ZIPARCHIVE::ER_RENAME'      => 'Renaming temporary file failed.',
            'ZIPARCHIVE::ER_CLOSE'       => 'Closing zip archive failed',
            'ZIPARCHIVE::ER_SEEK'        => 'Seek error',
            'ZIPARCHIVE::ER_READ'        => 'Read error',
            'ZIPARCHIVE::ER_WRITE'       => 'Write error',
            'ZIPARCHIVE::ER_CRC'         => 'CRC error',
            'ZIPARCHIVE::ER_ZIPCLOSED'   => 'Containing zip archive was closed',
            'ZIPARCHIVE::ER_NOENT'       => 'No such file.',
            'ZIPARCHIVE::ER_EXISTS'      => 'File already exists',
            'ZIPARCHIVE::ER_OPEN'        => "Can't open file",
            'ZIPARCHIVE::ER_TMPOPEN'     => 'Failure to create temporary file.',
            'ZIPARCHIVE::ER_ZLIB'        => 'Zlib error',
            'ZIPARCHIVE::ER_MEMORY'      => 'Memory allocation failure',
            'ZIPARCHIVE::ER_CHANGED'     => 'Entry has been changed',
            'ZIPARCHIVE::ER_COMPNOTSUPP' => 'Compression method not supported.',
            'ZIPARCHIVE::ER_EOF'         => 'Premature EOF',
            'ZIPARCHIVE::ER_INVAL'       => 'Invalid argument',
            'ZIPARCHIVE::ER_NOZIP'       => 'Not a zip archive',
            'ZIPARCHIVE::ER_INTERNAL'    => 'Internal error',
            'ZIPARCHIVE::ER_INCONS'      => 'Zip archive inconsistent',
            'ZIPARCHIVE::ER_REMOVE'      => "Can't remove file",
            'ZIPARCHIVE::ER_DELETED'     => 'Entry has been deleted',
        );

        $error_message = 'unknown';

        foreach ($zipFileFunctionsErrors as $constName => $em) {
            if (defined($constName) and constant($constName) === $errno) {
                $error_message = $em;
                break;
            }
        }

        $message = 'Zip File Function error: ' . $em;

        return $message;
    }

    function saveLogs($message, $opnDb = true)
    {
        if ($opnDb) {
            $opn = $this->openDB();
        }
        if (isset($_SESSION['evj_logId'])) {
            $aksi   = $this->get_data_unconnect("SELECT log_id,log_action FROM eva_logs WHERE log_id = " . $_SESSION['evj_logId']);
            $msg    = $aksi['log_action'] . ";" . $message;
            $this->exec_query_unconnect("UPDATE eva_logs SET log_action = '$msg' WHERE log_id = " . $aksi['log_id']);
        } else {
            $id = $this->get_data_unconnect("INSERT INTO eva_logs (log_action,log_user,log_level,log_logintime)
                VALUES('$message','" . $_SESSION['evj_id'] . "','" . $_SESSION['evj_kowil'] . "','NOW()') returning log_id");

            $_SESSION['evj_logId'] = $id['log_id'];
        }
        if ($opnDb) {
            $this->closeDB($opn);
        }
    }

    function getidKelompok($idne)
    {
        $sqll = $this->get_datas("select nama_kelompok from msr_mstrskoring where id = $idne");
        foreach ($sqll as $hasile) {
            echo $hasile['nama_kelompok'];
        }
    }

    function getDataKelompok($id)
    {
        $data = $this->get_data("SELECT nama_kelompok AS kelompok FROM msr_mstrskoring WHERE id = $id LIMIT 1");
        if (count($data) > 0) {
            return $data['kelompok'];
        } else {
            return '';
        }
    }

    function getKel($idkel = 0, $textFirst = '--Pilih Kategori--')
    {
        echo '<option value="0">' . $textFirst . '</option>';
        $sql = $this->get_datas("SELECT id AS id,nama_kelompok AS nampok FROM msr_mstrskoring ORDER BY id");
        foreach ($sql as $hasil) {
            $sel = ($hasil['id'] == $idkel) ? 'selected' : '';
            echo "<option value='" . $hasil['id'] . "'$sel>" . $hasil['nampok'] . "</option>";
        }
    }

    // function getrek($rek_kode = 0,$textFirst='--Pilih Rekening--'){
    //     echo '<option value="0">' . $textFirst . '</option>';
    //     $sql = $this->get_datas("SELECT rekening_code as rekcode,rekening_name as reknam from rekening");
    //     foreach ($sql as $hasil)
    //     {
    //         $sel = ($hasil['rekcode'] == $rek_code ) ? 'selected' : '';
    //         echo "<option value='" . $hasil['rekcode'] ."'$sel>" . $hasil['reknam'] ."</option>";
    //     }
    // }

    function cekStatusRenstra($idSkpd)
    {
        // mengecek apakah ada tujuan renstra yang masih tahap belum diverifikasi
        $cek = $this->get_data("SELECT count( tjr_id ) AS data_salah
                                FROM lkp_tujuanrenstra
                                WHERE
                                    tjr_uk_id = $idSkpd AND
                                    tjr_step != '4' ");

        if ($cek['data_salah'] > 0) {
            return false;
            exit;
        }
        // cek jika sudah tahap 4 atau verifikasi bappeda di cek lagi sudah di setujui atau belum
        $cek2 = $this->get_datas("SELECT tjr_status AS status FROM lkp_tujuanrenstra
                                    WHERE
                                        tjr_uk_id = $idSkpd AND
                                        tjr_step = '4' ");
        foreach ($cek2 as $val) {
            // jika ada status yang belum di verifikasi maka return false
            if ($val['status'] == '0') {
                return false;
                exit;
            }
        }
        return true;
    }
}
