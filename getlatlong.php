<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idpeng = $_POST['idpengawas'];

$getpaket = $db->get_datas("SELECT lang, long FROM pilah_paket where id_pengawas = $idpeng and lang is not null and long is not null");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'lat' => $paket['lang'],
        'long' => $paket['long'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
