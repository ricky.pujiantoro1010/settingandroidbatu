<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idrup = $_POST['id_rup'];

$getpaket = $db->get_datas("SELECT DISTINCT id_satker, kode_rup, tgl_mulai_perencanaan, tgl_akhir_perencanaan
FROM pilah_paket
WHERE kode_rup = $idrup");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'pkt_id' => $paket['kode_rup'],
        'id_satker' => $paket['id_satker'],
        'dateEnd' => $paket['tgl_akhir_perencanaan'],
        'dateStart' => $paket['tgl_mulai_perencanaan'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
