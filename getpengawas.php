<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idpengawas = $_POST['idpengawas'];

$getpaket = $db->get_datas("SELECT id_pengawas, nama_lengkap, nik, nomor, email, alamat_lengkap, foto_ktp
FROM data_pengawas where id_pengawas = $idpengawas");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'idpengawas' => $paket['id_pengawas'],
        'namapengawas' => $paket['nama_lengkap'],
        'fotoktp' => $paket['foto_ktp'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
