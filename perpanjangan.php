<?php

include "postgre.php";
include "edata.php";
include "class.objek.php";

$db = new db();
$edt = new edata();
$obj = new objek();

$idrup = $_POST['idrup'];

$getpaket = $db->get_datas("SELECT kode_rup, tgl_perpj_perencanaan from pilah_paket where kode_rup = $idrup");

$result = array();
foreach ($getpaket as $paket) {

    $tmp = array(
        'perpanjangan' => $paket['tgl_perpj_perencanaan'],
    );
    array_push($result, $tmp);
}

echo json_encode($result);
